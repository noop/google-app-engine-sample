package gaesample

import (
	"fmt"
	"net/http"
	"dathi"
)

func init() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/dathi", dathi.Dathi)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to the Machine!")
}
